import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDetailsModel, UserModel } from 'src/app/models/userModel';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-persona-view',
  templateUrl: './user-persona-view.component.html',
  styleUrls: ['./user-persona-view.component.scss']
})
export class UserPersonaViewComponent implements OnInit {
  editProfile: boolean = false;
  UserModel : UserModel = new UserModel()
  myUser: UserModel = new UserModel();
  UserId: any;
  constructor(private router: Router,private _userService : UserService) { }
  logout(){
    localStorage.clear();
    this.router.navigateByUrl("/login")
  }
  getUserDeatilsByUserId(userId){
    this._userService.GetUserById(userId).subscribe(
      data=>{
        this.myUser.id = data.id
        this.myUser.userId = data.userId
        this.myUser.firstName = data.firstName;
        this.myUser.middleName = data.middleName;
        this.myUser.lastName = data.lastName;
        this.myUser.emailId = data.emailId;
        this.myUser.mobileNo = data.mobileNo;
      this.UserModel = this.myUser
      }
    )
  }
  saveProfile(){
    this.UserModel.userId = this.myUser.userId
    this._userService.CreateUser(this.UserModel).subscribe(
      data=>{
      }
    )
  }
  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem("u_d"))
    this.UserId = ls.userId;
    this.getUserDeatilsByUserId(this.UserId)
  }

}
