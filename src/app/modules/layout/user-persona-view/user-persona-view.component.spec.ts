import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPersonaViewComponent } from './user-persona-view.component';

describe('UserPersonaViewComponent', () => {
  let component: UserPersonaViewComponent;
  let fixture: ComponentFixture<UserPersonaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPersonaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPersonaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
