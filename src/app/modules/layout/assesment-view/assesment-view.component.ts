import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { type } from 'os';
import { promise } from 'protractor';
import { RadioComponent } from 'src/app/components/radio/radio.component';
import {
  AnswerModel,
  AnswerReportModel,
  ReportModel,
} from 'src/app/models/answerModel';
import { QuestionModel } from 'src/app/models/questionModel';
import { AnswerService } from 'src/app/services/answer.service';
import { QuestionService } from 'src/app/services/question.service';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-assesments',
  templateUrl: './assesment-view.component.html',
  styleUrls: ['./assesment-view.component.scss'],
})
export class AssesmentViewComponent implements OnInit {
  questionBank: Array<any> = [];
  AnswerArray: Array<any> = [];
  userId = '';
  RetryQuestion: Array<any> = [];
  questionAttempted: number = 0;
  AllAnswersAreValid: boolean = false;
  AnswerReportModel: AnswerReportModel = new AnswerReportModel();
  ReportModel: ReportModel = new ReportModel();
  ListOfAnswerArray: Array<AnswerReportModel> = [];
  TypeOfAssesment: string = '';
  hashObj = {};
  //
  QuestionArray: Array<QuestionModel> = [];
  AnswersArray: Array<AnswerModel> = [];
  @ViewChildren(RadioComponent) items: any;
  ReportArray: any = [];
  constructor(
    private questionService: QuestionService,
    private answerService: AnswerService,
    private route: ActivatedRoute,
    private _reportService: ReportService,
    private router: Router,
    public dialog: MatDialog,
    private reportService: ReportService
  ) {}
  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem('u_d'));
    this.userId = ls.userId;
    this.isFirstAssesment()

    

    if (this.router.url == '/app/assesment/body') {
      this.TypeOfAssesment = 'body';
      this.generateBodyAssesment();
    } else {
      this.TypeOfAssesment = 'mind';
      this.generateMindAssesment();
    }
  }
  isFirstAssesment():boolean {
    this._reportService.getReportByUserId(this.userId).subscribe((data) => {
      if(data){
        const dialogRef = this.dialog.open(AssesmentPopUpFormComponent,{
          width: '350px',
          data: {name: this.userId, zone: data[0].zone}
        });

        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
        });
      }
      
      else{
        return true
      }
     
    });
    return false
  }
  generateBodyAssesment() {
    this.questionService.getBodyQuestions().subscribe((apiQ) => {
      this.answerService.getBodyAnswers().subscribe((apiA) => {
        apiQ.forEach((question) => {
          question.answer = [];
          apiA.forEach((answer) => {
            if (question.questionid == answer.questionId) {
              if (question.answer.length === 1) {
                if (question.answer[0].answer == 'Yes') {
                  question.answer.push(answer);
                } else {
                  var tenmp = question.answer[0];
                  question.answer = [];
                  question.answer.push(answer);
                  question.answer.push(tenmp);
                }
              } else {
                question.answer.push(answer);
              }
            }
          });
        });
      });
      this.questionBank = apiQ;
    });
  }
  generateMindAssesment() {
    this.questionService.getMindQuestions().subscribe((apiQ) => {
      this.answerService.getMindAnswers().subscribe((apiA) => {
        apiQ.forEach((question) => {
          question.answer = [];
          apiA.forEach((answer) => {
            if (question.questionid == answer.questionId) {
              question.answer.push(answer);
              question.answer.sort((a, b) => {
                return a.weight - b.weight;
              });
            }
          });
        });
      });
      this.questionBank = apiQ;
    });
  }
  countChangedHandler(ev) {
    this.hashObj[ev.questionId] = ev;
    this.questionAttempted = Object.keys(this.hashObj).length;
  }
  sumbit() {
    this.RetryQuestion = [];
    this.AnswerArray = [];
    // sorting Inputs into 2 part, result and error array
    this.items._results.forEach((element) => {
      if (element.sendAnswer) {
        this.AnswerArray.push(element.sendAnswer);
      } else {
        this.RetryQuestion.push(element.question);
      }
    });
    // Sending Error Message
    for (var i = 0; i < this.questionBank.length; i++) {
      for (var j = 0; j < this.RetryQuestion.length; j++) {
        if (
          this.questionBank[i].QuestionId === this.RetryQuestion[j].QuestionId
        ) {
          this.questionBank[i].isRed = true;
        }
      }
    }
    if (this.AnswerArray.length == this.questionBank.length) {
      // Sending Data to api
      this.SendReportToApi();
    }
  }

  async SendReportToApi() {
    if (this.TypeOfAssesment == 'body') {
      var weight = 0;
      this.AnswerArray.forEach((item) => {
        this.AnswerReportModel = new AnswerReportModel();
        this.AnswerReportModel.answerId = item.answerId;
        this.AnswerReportModel.userId = this.userId;
        this.AnswerReportModel.questionId = item.questionId;
        weight += item.weight;
        this.ListOfAnswerArray.push(this.AnswerReportModel);
      });
      await this.answerService
        .sendAnswer(this.ListOfAnswerArray)
        .subscribe((data) => console.log(data));

      if (weight == 0) {
        this.ReportModel.zone = 'Safe';
      } else if (weight >= 1 && weight < 5) {
        this.ReportModel.zone = 'Moderate';
      } else {
        this.ReportModel.zone = 'High';
      }

      this.ReportModel.userId = this.userId;
      this.reportService.addMasterReport(this.ReportModel).subscribe((data) => {
        console.log(data);
        this.reportService.addShareZone(this.ReportModel.zone);
        this.router.navigateByUrl('/app/assesment-complete/body');
      });
    } else if (this.TypeOfAssesment == 'mind') {
      this.AnswerArray.forEach((item) => {
        this.AnswerReportModel = new AnswerReportModel();
        this.AnswerReportModel.answerId = item.answerId;
        this.AnswerReportModel.userId = this.userId;
        this.AnswerReportModel.questionId = item.questionId;
        weight += item.weight;
        // console.log(this.AnswerReportModel,"AAA")
        this.ListOfAnswerArray.push(this.AnswerReportModel);
      });
      await this.answerService
        .sendAnswer(this.ListOfAnswerArray)
        .subscribe((data) => console.log(data));
      this.router.navigateByUrl('/app/assesment-complete/mind');
    } else {
      alert('Category Not Found');
    }
    //
  }
}
@Component({
  selector: 'previous-result',
  templateUrl: 'PreviousResultPopUp.html',
})
export class AssesmentPopUpFormComponent {
  ReportModel: ReportModel = new ReportModel()
  constructor(
    private reportService: ReportService,
    private router : Router,
    public dialogRef: MatDialogRef<AssesmentPopUpFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {name:string,zone:string}) {}


  repeatAnswer(){
    console.log(this.data)
    this.ReportModel.userId = this.data.name; 
    this.ReportModel.zone = this.data.zone
    console.log(this.ReportModel)
    this.reportService.addMasterReport(this.ReportModel).subscribe((data) => {
      console.log(data);
      this.reportService.addShareZone(this.ReportModel.zone);
      this.dialogRef.close()
      this.router.navigateByUrl('/app/assesment-complete/body');
  })}

  closeDialogeBox(){
    this.dialogRef.close();
  }
}