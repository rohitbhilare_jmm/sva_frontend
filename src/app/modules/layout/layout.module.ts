import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutViewComponent } from './layout-view/layout-view.component';
import { MatButtonModule } from '@angular/material/button';
import { GroupViewComponent } from './group-view/group-view.component';
import { UserPersonaViewComponent } from './user-persona-view/user-persona-view.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { ChartsModule } from 'ng2-charts';
import {ClipboardModule} from '@angular/cdk/clipboard';

import { PiechartComponent } from 'src/app/components/piechart/piechart.component';
import { RecentReportViewComponent } from 'src/app/components/recent-report-view/recent-report-view.component';
import { AssesmentPopUpFormComponent, AssesmentViewComponent } from './assesment-view/assesment-view.component';
import { RadioComponent } from 'src/app/components/radio/radio.component';
import {MatRadioModule} from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SmilecardComponent } from 'src/app/components/smilecard/smilecard.component';

import { GroupJoinComponent } from '../../components/group-join/group-join.component';
import { AssesmentSplashComponent } from 'src/app/components/assesment-splash/assesment-splash.component';
import {MatTabsModule} from '@angular/material/tabs';
import { GroupDetailDailogeComponent } from 'src/app/components/group-detail-dailoge/group-detail-dailoge.component';
import { GroupDialogeComponent } from 'src/app/components/group-dialoge/group-dialoge.component';
import { DummyReportViewComponent } from './dummyReport/report-view.component';

@NgModule({
  declarations: [LayoutViewComponent,    AssesmentSplashComponent,
    GroupDetailDailogeComponent,AssesmentPopUpFormComponent,
    GroupDialogeComponent,
    PiechartComponent,RecentReportViewComponent,SmilecardComponent,GroupJoinComponent,DummyReportViewComponent,
    GroupViewComponent,RadioComponent, UserPersonaViewComponent, DashboardViewComponent, AssesmentViewComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    ChartsModule,
    LayoutRoutingModule,
    MatIconModule,
    ClipboardModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule
  ]
})
export class LayoutModule { }
