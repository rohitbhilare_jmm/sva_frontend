import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GroupDialogeComponent } from 'src/app/components/group-dialoge/group-dialoge.component';
import { GroupService } from '../../../services/group.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupJoinComponent } from 'src/app/components/group-join/group-join.component';
import { ReportService } from 'src/app/services/report.service';
import { MindModel } from 'src/app/models/answerModel';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from 'src/app/models/userModel';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {
  UserId: string = ''
  lastAssessmentDate : any 
  CountOfGroups: number = 0
  RecentReport: Array<any>=[];
  UserModel : UserModel = new UserModel();
  showPersonalData: boolean = false;
  MindModel: MindModel = new MindModel()
  DepressionState: string = '';
  FirstTimeMindAnswers: boolean = false;
  AnxietyState: string = ''
  LonelinessState: string = ''
  ConfidenceState: string = ''
  constructor(
    private _groupService: GroupService,
    private _reportService : ReportService,
    public dialog: MatDialog,
    public _userService: UserService,
    private loc: Location,
    private route: ActivatedRoute,
    private router: Router
  ) { }
  getPersonalData(userId){
    this._reportService.getReportByUserId(userId).subscribe(
      data=>{
       this.RecentReport=  data
        this.lastAssessmentDate = data[0]
        console.log(this.lastAssessmentDate.createdDate)
        console.log(this.RecentReport)
        // this.RecentReport.forEach(element => {
        //   if(element.zone =="Safe"){
        //     element.Caption ="Safe"
        //   }
        //   if(element.zone =="Orange"){
        //     element.Caption ="Moderate"
        //   }
        //   if(element.zone =="Red"){
        //     element.Caption ="High"
        //   }

        // });
       this.showPersonalData= true
      }
    )
    }

  getUserByUserId(userId){
    this._userService.GetUserById(userId).subscribe(
      data=>{
        this.UserModel = data
      }
    )
  }
  openCreateGroupBox(){
    const dialogRef = this.dialog.open(GroupDialogeComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.getGroupsByUserId(this.UserId)
    });
  }
  openJoinGroupBox(groupId){
    const dialogRef = this.dialog.open(GroupJoinComponent,{
      data: {groupId:groupId }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.router.navigateByUrl("/dashboard")
    });
  }
  getGroupsByUserId(userId: string) {
    this._groupService.getGroupsByUserId(userId).subscribe(
      data => {
        this.CountOfGroups = data.length;
      }
    )
  }
  checkIfUserExistsinGroup(userId,groupId){
    if(userId && groupId){
      this._groupService.ValidateUserInGroup(userId,groupId).subscribe(
        data=>{
          if(data.length > 0){
            alert("You are already in that Group")
            localStorage.removeItem("joinGroup")
          }else{

            this.openJoinGroupBox(groupId)
          }
        })
    }else{
    }
  }
  getMindReport(userId){
   var  mindArray=new Object
    this._reportService.getMindReportByUserId(userId).subscribe(data=>{
      if(data.length > 0){
        this.FirstTimeMindAnswers = false;
      }else{
        this.FirstTimeMindAnswers = true;
      }
     data.forEach(element => {
      element.name = element.name.trim()
        if(mindArray.hasOwnProperty(element.name)){
          var key = element.name
          mindArray[element.name] =  mindArray[element.name] + element.weight
        }else{
          mindArray[element.name] = element.weight

          // mindArray[element.name] =element.weight
        }
     }); 
     if(mindArray["Anxiety"] >= 4){
       this.AnxietyState = "Take one step at a time and remember to pause"
     }else{
      this.AnxietyState = "pause, breathe and give yourself a pat on the back"

     }
     if(mindArray["Loneliness"] >= 4){
       this.LonelinessState = "Share what you feel with someone you are comfortable with"
     }else{
      this.LonelinessState = "People evolve, relationships evolve. spend more time with your loved ones and rediscover them"

     }
     if(mindArray["Confidence"] >= 4){
      this.ConfidenceState ="Set a small goal and achieve it!" 

     }else{
      this.ConfidenceState ="Focus on being the best version of yourself!" 

     }
     if(mindArray["Depression"] > 4){
      this.DepressionState ="Structure your day to keep you going"
    }else{
this.DepressionState = "try and focus on the things you enjoy doing and find that one thing that keeps you going"
     }
    })
    
  }
  ngOnInit(): void {
    // If User Logged IN 
    if(JSON.parse(localStorage.getItem("u_d"))){
      var ls = JSON.parse(localStorage.getItem("u_d"))
      this.UserId = ls.userId
      this.getUserByUserId(this.UserId)
      this.getGroupsByUserId(this.UserId);
      this.getPersonalData(this.UserId)
      this.getMindReport(this.UserId)
      // checks localStorage and Triggers Join Group
      var requestGroupId = localStorage.getItem("joinGroup")
      this.checkIfUserExistsinGroup(this.UserId,requestGroupId)
    }
    


// sets groupId into local storeage
  this.route.url.subscribe(data=>
    {
      if(data[0].path == "invite"){
      
        localStorage.setItem("joinGroup",data[1].path)
        this.router.navigateByUrl('login')
      }
    }
    )

  }

}
