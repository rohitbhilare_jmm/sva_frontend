import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssesmentSplashComponent } from 'src/app/components/assesment-splash/assesment-splash.component';
import { AuthGuard } from 'src/app/config/gaurd/auth.guard';
import { AssesmentViewComponent } from './assesment-view/assesment-view.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { DummyReportViewComponent } from './dummyReport/report-view.component';
import { GroupViewComponent } from './group-view/group-view.component';
import { LayoutViewComponent } from './layout-view/layout-view.component';
import { UserPersonaViewComponent } from './user-persona-view/user-persona-view.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutViewComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'invite/:id',
        component: DashboardViewComponent,
        // canActivate:[AuthGuard]
      },
      {
        path: 'assesment/:id',
        component: AssesmentViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'group',
        component: GroupViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'report',
        component: DummyReportViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'dummy',
        component: DummyReportViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'dummy/:id',
        component: DummyReportViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'report/:id',
        component: DummyReportViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'assesment',
        component: AssesmentViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'assesment-complete/:id',
        component: AssesmentSplashComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'user',
        component: UserPersonaViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
