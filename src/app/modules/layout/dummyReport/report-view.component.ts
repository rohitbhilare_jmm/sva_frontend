import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportFilterModel } from 'src/app/models/answerModel';
import { ReportPie } from 'src/app/models/reportModel';
import { GroupService } from 'src/app/services/group.service';
import { ReportService } from 'src/app/services/report.service';
@Component({
  selector: 'app-dummy-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.scss'],
  // host: {
  //   '(window:resize)': 'onWindowResize($event)',
  // },
})
export class DummyReportViewComponent implements OnInit {
  UserId: any;
  duration = ['7 days', '14 days'];
  selectedDuration = this.duration[1];
  ReportFilterModel: ReportFilterModel = new ReportFilterModel()
masterReports = []
  selectedGroupId = '';
  ListOfGroups: Array<any> = [];
  showPersonalData: boolean = false;
  DisplayData: boolean = false;
  RecentReport = [];
  PieChartDataObject = {};
  
  listofUsers=[];

  constructor(
    private _groupService: GroupService,
    private route: ActivatedRoute,
    private _reportService: ReportService,
    private _ref: ChangeDetectorRef
  ) {}

  getGroupListByUserId(userId) {
    this._groupService.getCarddetails(userId).subscribe((data) => {
      this.ListOfGroups = data;
    });
  }

  ShowLatestGroupReport() {
    this.ClearFilters();
    this.ListOfUsersInAGroup(this.selectedGroupId)
    let FilteredReport = []
    this._reportService
      .getReportByGroupId(this.selectedGroupId)
      .subscribe((AllReports) => {
        
        if(this.selectedDuration ==='7 days'){
        FilteredReport =    this.FilterReportsByDays(AllReports,7)
        }else{
        FilteredReport  =  this.FilterReportsByDays(AllReports,14)
        }
        console.log(FilteredReport)
        
        this.RenderRecentreportTemplate(FilteredReport);
        this.RenderPieChart(FilteredReport)
      });
  }
ListOfUsersInAGroup(groupId){
  this._groupService.getGroupDetails(this.selectedGroupId).subscribe(ListOfUsers => {
    this.listofUsers = ListOfUsers

  });
}
RenderPieChart(recentReportArray){
  console.log(this.PieChartDataObject)

  this.PieChartDataObject = new Object
  this.DisplayData = false
  console.log(this.PieChartDataObject)


  this.listofUsers.forEach(user => {
        let username = user.userId;
        this.PieChartDataObject[username] = ""
        recentReportArray.forEach(zone=>{
          this.PieChartDataObject[zone.userId] = zone.zone;
        })
        this.DisplayData = true

      });
      console.log(this.PieChartDataObject)

      this._ref.markForCheck(); 
}
clearPieChart(){
  this.PieChartDataObject = {}
  console.log("clearing Pie")
}


  RenderRecentreportTemplate(valueToInsert){
    this.RecentReport = valueToInsert;
    this.showPersonalData= true

  }

  FilterReportsByDays(reportArray,Days){
    let ReportList = []
    reportArray.forEach(  element => {
      const StartingDate  = new Date(Date.now() - Days * 24 * 60 * 60 * 1000)  // Tue Feb 04 2020 13:50:37 GMT...
      const eventDay =  new Date(element.createdDate)
      if(StartingDate <eventDay){
        ReportList.push(element)
        }
    });
    this.masterReports = ReportList
    return ReportList
  }
  RecentReportByColor(ev){
    this.ReportFilterModel.groupId= this.selectedGroupId;
    this.ReportFilterModel.zone = ev;
    console.log(this.ReportFilterModel);
    this._reportService.getReportByColorCategory(this.ReportFilterModel).subscribe(RecentReports => {
      if(RecentReports.length == 0){
        this.populateNotAssessedUsers()

      }else{
        this.populateAssesedUser(ev)
      }
    });
  }
populateAssesedUser(ev){
  let unAssesedUserModel = {
    userId: "",
    userName:'',
    zone: "",
    createdDate:""
}
let tempReport;
  let myReport = []
  this.listofUsers.forEach(async groupUser => {
    let userExisits = false;
    
    this.masterReports.forEach(reportUser =>{
      if(groupUser.userId === reportUser.userId){
        userExisits = true;
        tempReport = reportUser
      }
    })
    if(userExisits && tempReport.zone ==ev) {
      // console.log(groupUser,tempReport,"Exists with" ,ev)
      // unAssesedUserModel.userId = await tempReport.userId;
      // unAssesedUserModel.userName =await tempReport.userName;
      // unAssesedUserModel.zone = await ev;
      // unAssesedUserModel.createdDate = tempReport.createdDate;
      myReport.push(tempReport)
    }
  })
  console.log(myReport,"Final")
  this.RenderRecentreportTemplate(myReport)
}
  populateNotAssessedUsers(){
    console.log(this.masterReports, this.listofUsers)
    let unAssesedUserModel = {
        userId: "",
        userName:'',
        zone: ""
    }
    let myReport = []
    this.listofUsers.forEach(async groupUser => {
      let userExisits = false;
      this.masterReports.forEach(reportUser =>{
        if(groupUser.userId === reportUser.userId){
          userExisits = true;
        }
      })
      if(!userExisits){
        unAssesedUserModel.userId = await groupUser.userId;
        unAssesedUserModel.userName = await groupUser.firstName + " " + groupUser.lastName;
        unAssesedUserModel.zone = "Not Assessed";
        myReport.push(unAssesedUserModel)
      }

    })
    console.log(myReport)
    this.RenderRecentreportTemplate(myReport)

  }
  ClearFilters() {
    this.RecentReport = []
    this.DisplayData = false;
    this.showPersonalData= false;

  }

  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem('u_d'));
    this.UserId = ls.userId;
    this.getGroupListByUserId(this.UserId);
    this.showPersonalData= false;
    this.route.url.subscribe(async data => {
            this.selectedGroupId = data[1].path, "GroupIdByUrl";
            this.ShowLatestGroupReport();
          })
        }
  }

