// import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
// import { ReportFilterModel } from 'src/app/models/answerModel';
// import { ReportPie } from 'src/app/models/reportModel';
// import { GroupService } from 'src/app/services/group.service';
// import { ReportService } from 'src/app/services/report.service';
// @Component({
//   selector: 'app-dummy-view',
//   templateUrl: './report-view.component.html',
//   styleUrls: ['./report-view.component.scss'],
//   host: {
//     "(window:resize)":"onWindowResize($event)"
//   }
// })
// export class DummyReportViewComponent implements OnInit {
//   UserId: any;
//   ReportFilterModel: ReportFilterModel = new ReportFilterModel()

//   DisplayData: boolean = false;
//   showPersonalData: boolean = false;
//   showTotalGroupData: boolean = false;
//   showZonewiseData: boolean = false;
//   RecentReport: Array<any> = [];
//   DataDisplayArray : Array<any> = []
//   SendData = new Object;
//   ListOfGroups : Array<any> = []
//   duration = ["7 days","14 days"];
//   selectedGroupId="";
//   selectedDuration = this.duration[1]
//   listOfUsers: any;
//   constructor(private _groupService: GroupService, private route: ActivatedRoute
//     , private _reportService: ReportService) { }

//   ngOnInit(): void {
//     var ls = JSON.parse(localStorage.getItem("u_d"))
//     this.UserId = ls.userId
//     this.getGroupListByUserId(this.UserId)
//     this.getPersonalData(this.UserId);
//     this.route.url.subscribe(async data => {
//       this.selectedGroupId = data[1].path, "GroupIdByUrl";
//       this.ShowData();
//     })
//   }
//   getGroupListByUserId(userId) {
//     this._groupService.getCarddetails(userId).subscribe(
//       data => {
//         this.ListOfGroups = data
//       }
//     )
//   }
//   getPersonalData(userId) {
//     this._reportService.getReportByUserId(userId).subscribe(
//       data => {
//         this.RecentReport = data
//         this.showPersonalData = true;
//         this.showTotalGroupData = false;
//         this.showZonewiseData = false
//       }
//     )
//   }
//   async ShowData(){
//     this.SendData = {}
//     this.DisplayData = false;
//     this.showPersonalData = false;
//     this.showZonewiseData = false
//     this.showTotalGroupData = true;
//      this.DataDisplayArray = []
//     var SevenDay = [];
//     var FourteenDay = []
//     var SevenDayBackDate = new Date();
//     var pastDate = SevenDayBackDate.getDate() - 7;
//     this.listOfUsers = []
//     SevenDayBackDate.setDate(pastDate);

//      this._reportService.getReportByGroupId(this.selectedGroupId).subscribe(
//        Reportdata=>{
//          this._groupService.getGroupDetails(this.selectedGroupId).subscribe(  data => {
//           this.listOfUsers = data
//           Reportdata.forEach(element => {
//             var thatday = new Date(element.createdDate)

          
//             if (thatday >= SevenDayBackDate) {
//               SevenDay.push(element);
//               FourteenDay.push(element)
//             } else {
//               FourteenDay.push(element);
//             }
//           });
//           if(this.selectedDuration === "14 days"){
//             this.DataDisplayArray =  FourteenDay  
//           }else{
//             this.DataDisplayArray = SevenDay
//           }
//           this.listOfUsers.forEach(user => {
//             let username = user.userId;
//             this.SendData[username] = ""
//             this.DataDisplayArray.forEach(zone=>{
//               this.SendData[zone.userId] = zone.zone
//             })
//           });

//           this.populateGroupRecentTransections(this.DataDisplayArray)
//           this.DisplayData = true;
//         })      
      
//       }
//      )

  
//   }
//   RecentReportByColor(ev) {
//     this.showTotalGroupData = false
//     const tempRecentReports = this.RecentReport 
//     this.showPersonalData = false;
//     this.showZonewiseData = true
//     if (ev == "Grey") {
//       this.listOfUsers.forEach(user => {
//         var present = false
//         this.DataDisplayArray.forEach(report => {
//           if (user.userId == report.userId) {
//             present = true
//           } else {
//             present = false
//           }
//         })
//         if (present == false) {
//           this.RecentReport.push({ "userId": user.userId, "userName": user.firstName + " " + user.lastName, "zone": "Grey" })
//         }
//       });
//     } else {
//       this.ReportFilterModel.groupId = this.selectedGroupId;
//       this.ReportFilterModel.zone = ev
//       this.MapUsersWithRisk(tempRecentReports,ev)
//       // this._reportService.getReportByColorCategory(this.ReportFilterModel).subscribe(data => {
//       //   console.log(data, this.RecentReport)
//       //   // this.RecentReport = data
//       // })

//     }

//     this.showZonewiseData = true

//   }
//    async MapUsersWithRisk(arrayOfUsersWIthRisk,Risk){
//     let _map = {}
//     arrayOfUsersWIthRisk.sort(this.compare)
//     console.log(arrayOfUsersWIthRisk)
//     //  _map = Creating object of Users with their index of Recent Report
//     arrayOfUsersWIthRisk.forEach((element, index) => {
//       if(element.userId in _map){

//       }else{
//         _map[element.userId] = element.id
//       }
//     });
//     console.log(_map)
//     for (const key in _map) {
//       if (Object.prototype.hasOwnProperty.call(_map, key)) {
//         const element = _map[key];
//         console.log(element)
//       }
//     }
//     console.log(_map)
//    }


//    compare( a, b ) {
//     if ( a.createdDate < b.createdDate ){
//       return -1;
//     }
//     if ( a.createdDate > b.createdDate ){
//       return 1;
//     }
//     return 0;
//   }
//   populateGroupRecentTransections(arr) {
//     this.showPersonalData = false;
//     this.RecentReport = []
//     this.RecentReport.push(arr[arr.length-1])
//     this.RecentReport.push(arr[arr.length-2])
//     this.RecentReport.push(arr[arr.length-3])
//     this.RecentReport.push(arr[arr.length-4])
//     this.RecentReport.push(arr[arr.length-5])
//     this.showTotalGroupData = true;
//   }
//   }




