import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-layout-view',
  templateUrl: './layout-view.component.html',
  styleUrls: ['./layout-view.component.scss'],
  host: {
    "(window:resize)":"onWindowResize($event)"
  }
})
export class LayoutViewComponent implements OnInit {
  isMobile: boolean = false;
  width:number = window.innerWidth;
  height:number = window.innerHeight;
  mobileWidth:number  = 991;
  constructor(private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { 
    this.matIconRegistry.addSvgIcon(
      "mydashboard",
      this.domSanitizer.bypassSecurityTrustResourceUrl ("../../../../assets/svg/nav_dashboard.svg")
    );
    this.matIconRegistry.addSvgIcon(
      `mygroup`,
      this.domSanitizer.bypassSecurityTrustResourceUrl ("../../../../assets/svg/nav_groups.svg")
    );
    this.matIconRegistry.addSvgIcon(
      `myreport`,
      this.domSanitizer.bypassSecurityTrustResourceUrl ("../../../../assets/svg/nav_report.svg")
    );
    this.matIconRegistry.addSvgIcon(
      `myuser`,
      this.domSanitizer.bypassSecurityTrustResourceUrl ("../../../../assets/svg/nav_user.svg")
    );
    
  }
  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    if(this.width < this.mobileWidth){
      this.isMobile = true
    }else if(this.width > this.mobileWidth || this.width == this.mobileWidth){
      this.isMobile = false
    }
}
  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
  }

}
