import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GroupDetailDailogeComponent } from 'src/app/components/group-detail-dailoge/group-detail-dailoge.component';
import { GroupUserModel } from 'src/app/models/groupModel';
import { GroupService } from 'src/app/services/group.service';
import { GroupDialogeComponent } from '../../../components/group-dialoge/group-dialoge.component';
import { Location } from '@angular/common';
import { Clipboard } from '@angular/cdk/clipboard';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.scss']
})
export class GroupViewComponent implements OnInit {
  GroupUserModel: GroupUserModel = new GroupUserModel()
  listOfCards:any;
  listOfBg=['purple','green','yellow','red','blue','orange','lightblue','pink']
  shareLink:any;
  listOfUsers:Array<any> =[];
  UserId: any;
  constructor(public dialog: MatDialog,private rourter :Router,private _groupService: GroupService,private loc: Location,private clipboard: Clipboard) { }

  // inviteLink(event: ClipboardEvent){
  //   const {clipboardData} = event;

  //   const pastedText = clipboardData.getData();
  // }
  ViewReport(id){
    this.rourter.navigateByUrl("app/report/"+id)
  }
  alert(){
    alert("Link has been copied ")
  }
  createGroup(){
    const dialogRef = this.dialog.open(GroupDialogeComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getGroupUserCard(this.UserId)
      this.getAllGroups(this.UserId)
    });
  }
  viewGroup(groupId){
    const dialogRef = this.dialog.open(GroupDetailDailogeComponent,{
      data: {groupId:groupId }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getGroupUserCard(this.UserId)
      this.getAllGroups(this.UserId)
    });
  }
  getGroupUserCard(userId: string) {
    this._groupService.getGroupsByUserId(userId).subscribe(
      data => {
        this.GroupUserModel =data
       

      }
    )
  } 
 
  getAllGroups(userId){
    const angularRoute = this.loc.path();
    const url = window.location.href;
  const domainAndApp = url.replace(angularRoute, '');

    this._groupService.getCarddetails(userId).subscribe(
      data=>{
        data.forEach((element,i) => {
          element.InviteId = domainAndApp+"/app/invite/"+ element.groupId
          element.bg = this.listOfBg[i]
        });
        this.listOfCards = data
      }
    )
  }
 
   ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem("u_d"))
    this.UserId = ls.userId
    this.getGroupUserCard(this.UserId)
    this.getAllGroups(this.UserId)
  }

}
