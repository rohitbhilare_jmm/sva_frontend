import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from 'src/app/config/gaurd/login.guard';
import { LandingPageComponent } from './landing/landingPage';
import { LoginPageComponent } from './login-page/login-page.component';
import { PasswordPageComponent } from './passwordset/password-page.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'generatepassword/:id',
    component: PasswordPageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NologinRoutingModule { }
