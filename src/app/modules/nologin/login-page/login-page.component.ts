import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  UserForgotPassword,
  UserModel,
  UserValidation,
} from 'src/app/models/userModel';
import { UserService } from 'src/app/services/user.service';
import {
  FormGroup,
  FormControl,
  Validators,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
/** Error when the parent is invalid */
class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return control.dirty && form.invalid;
  }
}
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  UserModel: UserModel = new UserModel();
  ConfirmPassword: string = '';
  UserValidation: UserValidation = new UserValidation();
  foods: any[] = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' },
    { value: 'Others', viewValue: 'Others' },
  ];
  screenWidth: number;
  webView: boolean = false;
  mobileView: boolean = false;
  xlView: boolean;
  errorMatcher = new CrossFieldErrorMatcher();
  validLoginForm = new FormGroup({
    v_emailId: new FormControl('', [Validators.required, Validators.email]),
    v_password: new FormControl('', Validators.required),
  });
  signUpForm = new FormGroup(
    {
      firstName: new FormControl('', [Validators.required]),
      // middleName: new FormControl('', ),
      lastName: new FormControl('', Validators.required),
      mobileNo: new FormControl('', [
        Validators.required,
        Validators.pattern('[789][0-9]{9}'),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      gender: new FormControl('', Validators.required),
      // password: new FormControl('',Validators.required),
      // confirmPassword: new FormControl('',Validators.required),
    },
    {
      // validators: this.passwordValidator
    }
  );
  constructor(
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    public _userService: UserService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.onResize();
    this.toastyConfig.position = 'top-center';
  }

  onResize(event?) {
    if (window.innerWidth > 576) {
      this.webView = true;
    } else {
      this.mobileView = true;
    }
  }
  ngOnInit(): void {}

  login() {
    this.spinner.show();
    this.UserValidation.emailId = this.validLoginForm.get('v_emailId').value;
    this.UserValidation.password = this.validLoginForm.get('v_password').value;
    this._userService.ValidateUser(this.UserValidation).subscribe((data) => {
      if (data.message=="verified") {
        localStorage.setItem('u_d', JSON.stringify(data));
        this.toastyService.success({
          title: 'Logged in  !',
          showClose: true,
          timeout: 5000,
          theme: 'default',
        });
        this.spinner.hide();

        this.router.navigateByUrl('app/dashboard');
      } else if(data.message =="not valid") {
        this.spinner.hide();
        alert("Incorrect Password")
        this.UserValidation = new UserValidation();
        this.validLoginForm.reset()
      }
    });
  }
  clearSingUpFormData() {
    this.signUpForm.reset({
      email:{value:"", disabled: true},
      firstName:{value:"", disabled: true},
      gender:{value:"", disabled: true},
      lastName:{value:"", disabled: true},
      // middleName:{value:"", disabled: true},
      mobileNo:{value:"", disabled: true},
    })}


  
  signUpFormData() {
    this.spinner.show();
    this.UserModel.firstName = this.signUpForm.get('firstName').value;
    // this.UserModel.middleName = this.signUpForm.get('middleName').value;
    this.UserModel.lastName = this.signUpForm.get('lastName').value;
    this.UserModel.gender = this.signUpForm.get('gender').value;
    this.UserModel.emailId = this.signUpForm.get('email').value;
    this.UserModel.mobileNo = this.signUpForm.get('mobileNo').value;
    // this.UserModel.password = this.signUpForm.get('password').value
    //  this.validLoginForm.get('v_emailId').setValue = this.signUpForm.get('email').value
    //  this.validLoginForm.get('v_password').setValue = this.signUpForm.get('password').value
    this._userService.CreateUser(this.UserModel).subscribe((data) => {
      this.clearSingUpFormData();
      this.spinner.hide();
      if (data == 'Duplicate Email Id') {
        alert(
          'You have already signed up, try entering the password or click on forget Password'
        );
      } else {
        alert('Check Your Email to get Password, Please check spams folder.');
      }
    });
  }

  getUserById(userId) {
    this._userService.GetUserById(userId).subscribe((data) => {
      localStorage.setItem('userId', data.userId);
      this.router.navigateByUrl('/assesments');
    });
  }
  GetUserByEmailId(Email): any {
    this._userService.GetUserByEmailId(Email).subscribe((data) => {
      return data;
    });
  }
  openDialog() {
    const dialogRef = this.dialog.open(ForgetPasswordBox, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'forgotPassword.html',
  styleUrls: ['./login-page.component.scss'],
})
export class ForgetPasswordBox {
  EmailId: string = '';
  UserModel: UserModel = new UserModel();
  constructor(
    public dialogRef: MatDialogRef<ForgetPasswordBox>,
    public _userService: UserService,
    private spinner: NgxSpinnerService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  submitForgetPassword() {
    this.spinner.show();
    this._userService.GetUserByEmailId(this.EmailId).subscribe((data) => {
      this.UserModel.userId = data.userId;
      this.UserModel.emailId = this.EmailId;
      this._userService.ForgotPassword(this.UserModel).subscribe((datas) => {
        this.spinner.hide();
        if (datas == 'Success') {
          alert('Check Email for password Link');
          this.onNoClick();
        } else {
          alert('something went wrong, try again with valid email Id');
          this.onNoClick();
        }
      });
    });
  }
}
