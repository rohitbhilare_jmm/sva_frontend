import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NologinRoutingModule } from './nologin-routing.module';
import { ForgetPasswordBox, LoginPageComponent } from './login-page/login-page.component';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import { ToastyModule } from 'ng2-toasty';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PasswordPageComponent } from './passwordset/password-page.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import { LandingPageComponent } from './landing/landingPage';


@NgModule({
  declarations: [LoginPageComponent,LandingPageComponent,PasswordPageComponent,ForgetPasswordBox],
  imports: [
    CommonModule, 
    NologinRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSnackBarModule,
    ToastyModule.forRoot(),
    NgxSpinnerModule

  ]
})
export class NologinModule { }
