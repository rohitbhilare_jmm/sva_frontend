import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { UserModel, UserPassword } from 'src/app/models/userModel';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-password-page',
  templateUrl: './password-page.component.html',
  styleUrls: ['./password-page.scss']
})
export class PasswordPageComponent implements OnInit {
 
  UserPassword : UserPassword = new UserPassword();
  userId: string = ''
  passwordSetForm = new FormGroup({
  v_password: new FormControl('',Validators.required),
  c_password: new FormControl('',[Validators.required,]),
});
constructor(
  public _userService: UserService,private route: ActivatedRoute,private router :Router, private toastyConfig: ToastyConfig,private _snackBar: MatSnackBar
){
    this.toastyConfig.theme ='material',
    this.toastyConfig.position ='top-center'
}
  ngOnInit(): void {
    this.route.url.subscribe(async data => {
      this.passwordSetForm.get("v_password").setValue("")
      this.passwordSetForm.get("c_password").setValue("")
      this.userId = data[1].path
    })
  }
  generatePassword(){
if(this.passwordSetForm.get('v_password').value === this.passwordSetForm.get('c_password').value){
  this.UserPassword.password = this.passwordSetForm.get('v_password').value
  this.UserPassword.userId = this.userId;
  this._userService.generatePassword(this.UserPassword).subscribe(
    data=>{
      alert("Password Changed, Try Login with New Password")
      this._snackBar.open("Password Changed", "close", {
        duration: 2000,
      });    
      this.router.navigateByUrl('/login')
    
    }
  )
}else{
  this.passwordSetForm.get("v_password").setValue("")
  this.passwordSetForm.get("c_password").setValue("")
  this._snackBar.open("Password MisMatch, try again", "close", {
    duration: 2000,
  });    
}

    

  }
}
