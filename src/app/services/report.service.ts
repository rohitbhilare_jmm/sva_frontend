import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { Observable } from 'rxjs';
import { ReportFilterModel, ReportModel } from '../models/answerModel';

@Injectable({
    providedIn: 'root'
  })
  export class ReportService { 
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    ReportModel : ReportModel = new ReportModel(); 
    zone = "";
    ReportFilterModel : ReportFilterModel = new ReportFilterModel(); 
    constructor(private httpClient: HttpClient,
        private route: Router) {
            this.apiUrl = this.AppConfig.apiUrl
        }

    public addMasterReport(reportModel: ReportModel):Observable<any>{
        return this.httpClient.post<any>(this.apiUrl+'Report/',reportModel);
    }
    
    public getReportByGroupId(groupid: string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'Report'+"/"+groupid);
    }
    public getReportByUserId(userId: string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'Report/user'+"/"+userId);
    }
    public getMindReportByUserId(userId: string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'Report/mind'+"/"+userId);
    }
    public getReportByColorCategory(ReportFilterModel: ReportFilterModel):Observable<any>{
        return this.httpClient.post<any>(this.apiUrl+'Report/Filter',ReportFilterModel);
    }
    public addShareZone(result:string){
        this.zone = result
        return result
    }
    public getShareZone(){
        return this.zone
    }
} 