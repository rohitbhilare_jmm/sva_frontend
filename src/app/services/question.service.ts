import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { QuestionModel } from '../models/questionModel';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
  })
  export class QuestionService { 
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    QuestionModel : QuestionModel = new QuestionModel();
    constructor(private httpClient: HttpClient,
        private route: Router) {
            this.apiUrl = this.AppConfig.apiUrl
        }

   
    public getBodyQuestions():Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'question/body');
    }
    public getMindQuestions():Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'question/mind');
    }
  
} 