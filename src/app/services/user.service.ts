import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { Observable } from 'rxjs';
import { UserModel, UserValidation ,UserPassword} from '../models/userModel';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    UserModel: UserModel = new UserModel()
    UserPassword: UserPassword = new UserPassword()
    UserValidation: UserValidation = new UserValidation()
    UserLogggedIn: boolean = false
    constructor(
        private httpClient: HttpClient,
        private route: Router

    ) {
        this.apiUrl = this.AppConfig.apiUrl
    }

 
    public GetUserById(id: string): Observable<any> {
        return this.httpClient.get<any>(this.apiUrl + 'user/' + id);
    }
    public GetUserByEmailId(emailId: string): Observable<any> {
        return this.httpClient.get<any>(this.apiUrl + 'user/certified/' + emailId);
    }
    public CreateUser(UserModel: UserModel)  {
        return this.httpClient.post(this.apiUrl + 'User', UserModel,{responseType: 'text'});
    }  
    public ForgotPassword(UserModel: UserModel)  {
        return this.httpClient.post(this.apiUrl + 'User/RetrivePassword', UserModel,{responseType: 'text'});
    }  
 
    public generatePassword(UserPassword: UserPassword): Observable<any>   {
        return this.httpClient.post<any>(this.apiUrl + 'User/generatePassword', UserPassword);
    }  
 
    public ValidateUser(UserValidation: UserValidation): Observable<any> {
        return this.httpClient.post<any>(this.apiUrl + 'User/validate', UserValidation)
    }

} 