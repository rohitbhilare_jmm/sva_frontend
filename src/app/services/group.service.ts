import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { Observable } from 'rxjs';
import { GroupMasterModel, GroupUserModel } from '../models/groupModel';
@Injectable({
    providedIn: 'root'
  })
  export class GroupService { 
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    GroupMasterModel : GroupMasterModel = new GroupMasterModel();
    GroupUserModel : GroupUserModel = new GroupUserModel();
    constructor(private httpClient: HttpClient,
        private route: Router) {
            this.apiUrl = this.AppConfig.apiUrl
        }

   
    public deleteUserFromGroup(GroupUserModel: GroupUserModel):any{
        return this.httpClient.post<any>(this.apiUrl+'usergroup/removefromgroup/',GroupUserModel);
    }
    public createGroup(GroupMasterModel:GroupMasterModel):any{
        return this.httpClient.post<any>(this.apiUrl+'GroupMaster/',GroupMasterModel);
    }
    public addUserToGroup(GroupUserModel:GroupUserModel){
        return this.httpClient.post(this.apiUrl+'UserGroup/',GroupUserModel);
    }
    public getGroupDetailsByGroupId(groupId: string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'groupmaster/' + groupId);
    }
    public getGroupsByUserId(id: string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'UserGroup/' + id);
    }
    public getUserByGroupId(id:string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl +'UserGroup/users/' + id)
    }
    public getCarddetails(userId:string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl +'usergroup/Card/' + userId)
    }
    public getGroupDetails(groupId:string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl +'usergroup/Group/' + groupId)
    }
    public ValidateUserInGroup(userId: string,groupId:string):Observable<any>{
        return this.httpClient.get<any>(this.apiUrl +'usergroup/validate' + "/"+userId+'/'+groupId)
    }

} 