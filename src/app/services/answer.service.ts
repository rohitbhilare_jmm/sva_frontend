import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { Observable } from 'rxjs';
import { AnswerModel, AnswerReportModel } from '../models/answerModel';
@Injectable({
    providedIn: 'root'
  })
  export class AnswerService { 
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    AnswerModel : AnswerModel = new AnswerModel(); 
    AnswerReportModel : AnswerReportModel = new AnswerReportModel(); 
    constructor(private httpClient: HttpClient,
        private route: Router) {
            this.apiUrl = this.AppConfig.apiUrl
        }

   
    public getBodyAnswers():Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'answer/categorywise/body');
    }
    public getMindAnswers():Observable<any>{
        return this.httpClient.get<any>(this.apiUrl+'answer/categorywise/mind');
    }
    public sendAnswer(answerReportModel: any):Observable<any>{
        return this.httpClient.post<any>(this.apiUrl+'AnswerReport/',answerReportModel);
    }
} 