import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config/apiConfig/api.config';
import { Observable } from 'rxjs';
import { GroupMasterModel, GroupUserModel } from '../models/groupModel';
import { UserValidation } from '../models/userModel';
@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public apiUrl: string;
    AppConfig: AppConfig = new AppConfig();
    UserValidation: UserValidation = new UserValidation();
    UserSingedIn: boolean = false;
    constructor(private httpClient: HttpClient,
        private route: Router) {
        this.apiUrl = this.AppConfig.apiUrl
    }
    public isAuthenticated(UserValidation: UserValidation): any {
        return this.httpClient.post<any>(this.apiUrl + 'user/validate', UserValidation);
    }

} 