import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssesmentSplashComponent } from './assesment-splash.component';

describe('AssesmentSplashComponent', () => {
  let component: AssesmentSplashComponent;
  let fixture: ComponentFixture<AssesmentSplashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssesmentSplashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssesmentSplashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
