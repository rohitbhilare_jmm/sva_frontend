import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MindModel } from 'src/app/models/answerModel';
import { Spash } from 'src/app/models/reportModel';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-assesment-splash',
  templateUrl: './assesment-splash.component.html',
  styleUrls: ['./assesment-splash.component.scss']
})
export class AssesmentSplashComponent implements OnInit {
  Spash: Spash = new Spash()
  ShowBody: boolean = false;
  UserId: any;
  MindModel: MindModel = new MindModel()
  DepressionState: string = ''
  AnxietyState: string = ''
  LonelinessState: string = ''
  ConfidenceState: string = ''
  constructor(private _reportService: ReportService,private router : ActivatedRoute,private route: Router) { }
getzone(){
  var myzone = this._reportService.getShareZone()
  if(myzone == "Moderate"){
    this.Spash.Tittle = "Medium Risk of infection";
    this.Spash.bigImage = "../../../assets/svg/yellow.svg"
    this.Spash.img = "../../../assets/svg/yellow_icon.svg"
    this.Spash.Caption ="Your answers indicate that you have possible symptoms of COVID-19. Based on this information, you should speak to your health care provider. You may be directed to a testing site"
    this.Spash.Guidelines=["Remain calm","Speak to your healthcare provider","Monitor symtoms",'Wear a mask when around others, if contact is necessary',"If symptoms continue or get worse, or if new symptoms start, contact your health care provider"]
  }else if (myzone == "Safe"){
    this.Spash.Tittle = "Safe";
    this.Spash.bigImage = "../../../assets/svg/green.svg"
    this.Spash.img = "../../../assets/svg/green_icon.svg"
    this.Spash.Caption ="Your answers indicate that you do not have any symptoms or other factors that indicate an infection"
    this.Spash.Guidelines = ["Remain cautious","Wear a mask","Manitain social distancing",'Wash your hands frequently',"Use a sanitiser when you're unable to wash your hands."]

  }else if (myzone == "High"){
    this.Spash.Tittle = "High risk of infection";
    this.Spash.bigImage = "../../../assets/svg/red.svg"
    this.Spash.img = "../../../assets/svg/red_icon.svg"
    this.Spash.Caption = "Your answers indicate that you have possible symptoms of COVID-19 and that you've had close contact with someone who has the condition. Based on this information, you should speak to your health care provider. You may be directed to a testing site"
    this.Spash.Guidelines = ["Remain calm","Speak to you healthcare provider; they may recommend getting tested for COVID-19"
  ,"Don't leave your home, except to get medical care","Stay in a specific room away from others in your home, and use a separate bathroom, if possible"
,"Do not share household items","Wear a mask when around others, if contact is necessary",'If symptoms continue or get worse, your healthcare provider may recommend hospitalisation']
  }


}

getMindReport(userId){
  var  mindArray=new Object
   this._reportService.getMindReportByUserId(userId).subscribe(data=>{
    data.forEach(element => {
     element.name = element.name.trim()
       if(mindArray.hasOwnProperty(element.name)){
         var key = element.name
         mindArray[element.name] =  mindArray[element.name] + element.weight
       }else{
         mindArray[element.name] = element.weight

       }
    }); 
    if(mindArray["Anxiety"] >= 4){
      this.AnxietyState = "Take one step at a time and remember to pause"
    }else{
     this.AnxietyState = "pause, breathe and give yourself a pat on the back"

    }
    if(mindArray["Loneliness"] >= 4){
      this.LonelinessState = "Share what you feel with someone you are comfortable with"
    }else{
     this.LonelinessState = "People evolve, relationships evolve. spend more time with your loved ones and rediscover them"

    }
    if(mindArray["Confidence"] >= 4){
     this.ConfidenceState ="Set a small goal and achieve it!" 

    }else{
     this.ConfidenceState ="Focus on being the best version of yourself!" 

    }
    if(mindArray["Depression"] > 4){
     this.DepressionState ="Structure your day to keep you going"
   }else{
this.DepressionState = "try and focus on the things you enjoy doing and find that one thing that keeps you going"
    }
   })
   
 }
  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem("u_d"))
    this.UserId = ls.userId
    this.router.url.subscribe(
      data=>{
        if(data[1].path == 'body'){
          this.getzone()
          this.ShowBody = true;
        }else if(data[1].path == 'mind'){
          this.ShowBody = false;
          this.getMindReport(this.UserId)
        }else{
          this.route.navigateByUrl('dashboard')
          
        }
      }
    )
  }

}
