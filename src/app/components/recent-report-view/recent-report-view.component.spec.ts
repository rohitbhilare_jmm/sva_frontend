import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentReportViewComponent } from './recent-report-view.component';

describe('RecentReportViewComponent', () => {
  let component: RecentReportViewComponent;
  let fixture: ComponentFixture<RecentReportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentReportViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
