import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-recent-report-view',
  templateUrl: './recent-report-view.component.html',
  styleUrls: ['./recent-report-view.component.scss']
})
export class RecentReportViewComponent implements OnInit {
  @Input() recentReportArray: any;
  showName= true;
  constructor(private router : Router) { }

  ngOnInit(): void {
    console.log(this.recentReportArray)
    // this.recentReportArray.forEach(element => {
    //   if(element.zone =="Safe"){
    //     element.Caption ="Safe"
    //   }
    //   if(element.zone =="Orange"){
    //     element.Caption ="Moderate"
    //   }
    //   if(element.zone =="Red"){
    //     element.Caption ="High"
    //   }

    // });
    if(this.router.url === "/app/dashboard")
    {
      this.showName = false
    }   
  }

}
