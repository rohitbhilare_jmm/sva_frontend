import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { GroupMasterModel, GroupUserModel } from 'src/app/models/groupModel';
import{GroupService} from '../../services/group.service';
@Component({
  selector: 'app-group-dialoge',
  templateUrl: './group-dialoge.component.html',
  styleUrls: ['./group-dialoge.component.scss']
})
export class GroupDialogeComponent implements OnInit {
  GroupMasterModel: GroupMasterModel = new GroupMasterModel()
  GroupUserModel: GroupUserModel = new GroupUserModel()
  UserId: any;
  constructor( private GroupService : GroupService,
    public dialogRef: MatDialogRef<GroupDialogeComponent>) {
    
   }
  newGroup(){
    if(!this.GroupMasterModel.name){
      alert("Could not create group with no name ")  
      return 
    }
    this.GroupService.createGroup(this.GroupMasterModel).subscribe(
      
      data=>{
        this.GroupUserModel.groupId = data;
        this.GroupUserModel.userId = this.UserId
        this.GroupService.addUserToGroup(this.GroupUserModel).subscribe(data=>console.log(data))
          this.dialogRef.close();

      }
    )

  }
  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem("u_d"))
    this.UserId = ls.userId
  }

}
