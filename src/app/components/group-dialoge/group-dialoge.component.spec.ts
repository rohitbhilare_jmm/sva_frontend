import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDialogeComponent } from './group-dialoge.component';

describe('GroupDialogeComponent', () => {
  let component: GroupDialogeComponent;
  let fixture: ComponentFixture<GroupDialogeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupDialogeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDialogeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
