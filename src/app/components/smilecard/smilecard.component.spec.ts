import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmilecardComponent } from './smilecard.component';

describe('SmilecardComponent', () => {
  let component: SmilecardComponent;
  let fixture: ComponentFixture<SmilecardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmilecardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmilecardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
