import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-smilecard',
  templateUrl: './smilecard.component.html',
  styleUrls: ['./smilecard.component.scss']
})
export class SmilecardComponent implements OnInit {
  MyMood: string;
  CardDisplay = {
    name:"",
    bgimg:''
  };
cardChange=[
  {
    "img":'../../../assets/icons/happy.svg',
    "name":"Ecstatic",
    "background-color":"#4fec9f",
    "bgimg":"../../../assets/icons/Great_Big_Smiley.png"
  },
  {
    "img":'../../../assets/icons/MildHappy.svg',
    "name":"Happy",
    "background-color":"#a7eff3",
    "bgimg":"../../../assets/icons/Happy_Big_Smiley.png"
    
  },
  {
    "img":'../../../assets/icons/So-So.svg',
    "name":"So-So",
    "background-color":"#feee68",
    "bgimg":"../../../assets/icons/Okay_Big_Smiley.png"
  },
  {
    "img":'../../../assets/icons/Unhappy.svg',
    "name":"Unhappy",
    "background-color":"#fdcd6e" ,
    "bgimg":"../../../assets/icons/Sad_Big_Simley.png"
  },
  {
    "img":'../../../assets/icons/Sad.svg',
    "name":"Sad",
    "background-color":"#ff7f7d",
    "bgimg":"../../../assets/icons/Upset_Big_Smiley.png"

  },
]
  constructor() { }

  changeCard(moodName){
    this.CardDisplay.name = moodName;
    this.cardChange.forEach(element => {
      if(element.name == this.CardDisplay.name){
        this.CardDisplay.bgimg = element.bgimg
        // this.CardDisplay.bgimg= element.background-img
      }
    });
  }
  changeMood(ev){
    this.MyMood = ev.name
    localStorage.setItem("mood",ev.name)
    this.changeCard(this.MyMood)
  }
  ngOnInit(): void {
    this.MyMood =  localStorage.getItem("mood")
    if(this.MyMood){
      this.changeCard(this.MyMood)

    }else{
      this.MyMood = "So-So"
    this.changeCard(this.MyMood)

    }
  }

}
