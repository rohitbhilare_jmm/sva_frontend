import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {
  @Input() question: any;
  @Output() sendData: EventEmitter<any> = new EventEmitter<any>();
  cartoonControl = new FormControl('', [Validators.required]);

  sendAnswer: any;
  constructor() { }
  sendValue(ev){  
    
    this.question.isRed = false;
    this.sendData.emit(ev.value)
  } 
  ngOnInit(): void {
    
  }
  
}

