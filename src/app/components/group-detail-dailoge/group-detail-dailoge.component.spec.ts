import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDetailDailogeComponent } from './group-detail-dailoge.component';

describe('GroupDetailDailogeComponent', () => {
  let component: GroupDetailDailogeComponent;
  let fixture: ComponentFixture<GroupDetailDailogeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupDetailDailogeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDetailDailogeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
