import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GroupUserModel } from 'src/app/models/groupModel';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-group-detail-dailoge',
  templateUrl: './group-detail-dailoge.component.html',
  styleUrls: ['./group-detail-dailoge.component.scss']
})
export class GroupDetailDailogeComponent implements OnInit {
  listOfUsers:Array<any>=[]
  backgroundColor = "";
  groupName= ""
  GroupUserModel:GroupUserModel = new GroupUserModel()
  userId: any;
  myGroupId: string;
  constructor(
    public dialogRef: MatDialogRef<GroupDetailDailogeComponent>,
    private _groupService: GroupService,
    private router : Router,
    @Inject(MAT_DIALOG_DATA) public groupId: any) { }
    getGroupDetails(groupId){
      this._groupService.getGroupDetails(groupId).subscribe(
        data=>{
          this.listOfUsers = data
        }
      )
    }
  ngOnInit(): void {
    this.groupName = this.groupId.groupId.name
    this.backgroundColor = this.groupId.groupId.bg
    this.getGroupDetails(this.groupId.groupId.groupId)
    this.userId =  JSON.parse(localStorage.getItem('u_d')).userId
  }
  exitgroup(){
    this.myGroupId = this.groupId.groupId;
    this.GroupUserModel.userId = this.userId;
    this.GroupUserModel.groupId = this.groupId.groupId.groupId;
    this.GroupUserModel.isDeleted = true;
    this.GroupUserModel.isActive = false

    this.GroupUserModel.id = this.groupId.id
    this._groupService.deleteUserFromGroup(this.GroupUserModel).subscribe(data => {
      this.dialogRef.close();
    })
  }
}
