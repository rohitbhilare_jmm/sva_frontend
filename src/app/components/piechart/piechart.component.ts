import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {
  @Input() childData: any;
  @Output() onColorSelection: EventEmitter<any> = new EventEmitter<any>();

  redCounter: number = 0;
  greenCounter: number = 0;
  orangeCounter:number = 0;
  greyCointer: number = 0
  
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    
    plugins: {
      // datalabels: {
      //   formatter: (value, ctx) => {
      //     const label = ctx.chart.data.labels[ctx.dataIndex];
      //     return label;
      //   },
      // },
      datalabels: {
        formatter: () => {
          return null;
        },
      },
    }
  };
  public pieChartLabels: Label[] = [['High risk of infection'], ['Safe'], ['Medium Risk of infection'],['Not Assessed']];
  public pieChartData: number[] = [] ;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255, 74, 69)', 'rgba(40, 216, 134)', 'rgba(255,213,26)', 'rgba(224, 224, 224)'],
    },
  ];
  constructor(private _groupService: GroupService) { }
generateGraphs(obj){
  var array : Array<any> =[]
  for (const property in obj) {
    array.push({"userId":property,"zone":`${obj [property]}` })

  }
  this.pieChartData = []
array.forEach(element => {
  if(element.zone =="High"){
    this.redCounter = this.redCounter +1
  }else if(element.zone =="Safe"){
    this.greenCounter = this.greenCounter +1
  }else if(element.zone =="Moderate"){
    this.orangeCounter = this.orangeCounter +1
  }
  else {
    this.greyCointer = this.greyCointer +1
  }
});
this.pieChartData = [this.redCounter,this.greenCounter,this.orangeCounter,this.greyCointer ]
}

  ngOnInit(): void {
    console.log(this.childData,"Somethin")
    this.generateGraphs(this.childData)
  }
  chartClicked(ev){
    console.log(ev.active[0]._view.label[0])
    if(ev.active[0]._view.label[0] ==="Safe"){
      this.onColorSelection.emit("Safe");
    }
    if(ev.active[0]._view.label[0] ==="High risk of infection"){
      this.onColorSelection.emit("High");
    }
    if(ev.active[0]._view.label[0] ==="Medium Risk of infection"){
      this.onColorSelection.emit("Moderate");
    }
    if(ev.active[0]._view.label[0] ==="Not Assessed"){
      this.onColorSelection.emit("Grey");
    }
    
    // this.onColorSelection.emit(ev.active[0]._view.label[0]);

  }
}
