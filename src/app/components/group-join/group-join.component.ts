import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GroupDialogeComponent } from '../group-dialoge/group-dialoge.component';
import { GroupService } from '../../services/group.service';
import { GroupUserModel } from 'src/app/models/groupModel';

@Component({
  selector: 'app-group-join',
  templateUrl: './group-join.component.html',
  styleUrls: ['./group-join.component.scss']
})
export class GroupJoinComponent implements OnInit {
  GroupName: string;
  GroupUserModel: GroupUserModel = new GroupUserModel()
  UserId: any;
  constructor(public dialogRef: MatDialogRef<GroupJoinComponent>,
    private _groupService: GroupService,
    @Inject(MAT_DIALOG_DATA) public groupId: any) { }

  getGroupInfoByGroupId(groupId) {
    this._groupService.getGroupDetailsByGroupId(groupId).subscribe(
      data => {
        this.GroupName = data.name
      }
    )
  }
  accept() {
    this.GroupUserModel.groupId = this.groupId.groupId;
    this.GroupUserModel.userId = this.UserId;

    this._groupService.addUserToGroup(this.GroupUserModel).subscribe(data => {
      localStorage.removeItem("joinGroup")
    }
    )

    this.dialogRef.close();
  }
  decline() {
    localStorage.removeItem("joinGroup")
    this.dialogRef.close();
  }
  ngOnInit(): void {
    var ls = JSON.parse(localStorage.getItem("u_d"))
    this.UserId = ls.userId
    this.getGroupInfoByGroupId(this.groupId.groupId)
  }

}
