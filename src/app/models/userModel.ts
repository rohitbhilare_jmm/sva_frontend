export class UserModel {
    id: number;
    userId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    gender: string;
    emailId: string;
    password: string;
    mobileNo: string;
    hostIp: string;
    createdBy: string;
    creationDate: Date;
    updatedBy: string;
    updatedDate: Date;
    isActive: boolean;
    isDeleted: boolean;
} 
export class UserDetailsModel {
    userId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    gender: string;
    emailId: string;
    password: string;
    mobileNo: string;
    createdBy: string;
    creationDate: Date;
    updatedBy: string;
    updatedDate: Date;

} 
export class UserValidation{
    emailId: string;
    password: string;
}
export class UserPassword{
    userId: string;
    password: string;
}
export class UserForgotPassword{
    userId: string;
    emailId: string;
    password: string;
}