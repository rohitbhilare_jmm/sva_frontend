export class AnswerModel {
     id:number;
     answerId  :string;
     questionId  : string;
     answer :  string ;
     weight  :number;
     hostIp  :string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     isActive :boolean; 
     isDeleted :boolean;
} 
export class AnswerReportModel {
     id:number;
     reportId  :string;
     userId  :string;
     questionId  : string;
     answerId :  string ;
     hostIp  :string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     isActive :boolean; 
     isDeleted :boolean;
} 

export class ReportModel {
     id:number;
     userId  :string;
     groupId  : string;
     zone :  string ;
     hostIp  :string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     isActive :boolean; 
     isDeleted :boolean;
}
export class ReportFilterModel {
    
     groupId  : string;
     zone :  string ;
    
}
export class MindModel {
    
     name  : string;
     question :  string ;
     questionCategoryId :  string ;
     userId :  string ;
     weight: number;
     createdDate: Date;
}