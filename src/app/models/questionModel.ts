export class QuestionModel {
    id:number;
    questionId  :string;
    questionCategoryId  : string;
    question :  string ;
    
     hostIp  :string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     isActive :boolean; 
     isDeleted :boolean;
} 