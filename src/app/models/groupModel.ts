export class GroupMasterModel {
     id:number;
     groupId  :string;
     name  : string;
     hostIp  :string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     usActive :boolean; 
     usDeleted :boolean;
} 
export class GroupUserModel {
     id:number;
     groupId  :string;
     userId  : string;
     hostIp  :string;
     inviteId:string;
     createdBy  :string;
     creationDate :Date;
     updatedBy  :string;
     updatedDate  :Date;
     isActive :boolean; 
     isDeleted :boolean;
} 
export class GroupUserMicroModel {
     id:number;
     groupId  :string;
     isAdmin  : boolean;
     name  :string;
     userId  :string;
   
} 

