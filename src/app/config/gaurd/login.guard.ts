import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Injectable()
export class LoginGuard implements CanActivate {


  constructor(private _authService: UserService, private _router: Router) {
  }


  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    if (localStorage.getItem('u_d')) {
      this._router.navigate(['/dashboard']);
      return false;
    }

    // navigate to login page
    // you can save redirect url so after authing we can move them back to the page they requested
    return true;
  }

}