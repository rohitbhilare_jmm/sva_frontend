export class AppConfig {
    public readonly url = 'https://api.svalife.me';
    
    public readonly apiUrl = this.url + '/api/v1/';
}
// export class AppConfig {
//     public readonly url = 'https://localhost:';
//     public readonly port = '5001';
//     public readonly host = this.url + this.port;
//     public readonly apiUrl = this.host + '/api/v1/';
// }
